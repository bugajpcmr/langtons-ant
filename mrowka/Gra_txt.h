//--------------------------------------
// Klasa Gra_txt
//
// okresla jak ma zachowywac sie gra w srodowisku tekstowym, 
// zawiera metode wirtualna do wyswietlania planszy (tablicy 2D)
// dziedziczy z klasy Gra
//
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 23/04/2019
//

#ifndef GRA_TXT_H_
#define GRA_TXT_H_

#include "Gra.h"

class Gra_txt : public Gra
{
public:
	Gra_txt(int i, int j);
	~Gra_txt();

	void View();
};

#endif

