//--------------------------------------
// Klasa Mrowka
//
// zawiera wspolrzedne mrowki
//
// Autor: Bartosz Bugajski
//
// Historia zmian:
// Data utworzenia: 23/04/2019
//

#ifndef MROWKA_H_
#define MROWKA_H_

class Mrowka
{
public:
	Mrowka();
	~Mrowka();

	//gettery
	int GetX();
	int GetY();
	const char GetZwrot();

	//settery
	void SetX(int x);
	void SetY(int y);
	void SetZwrot(const char y);

private:
	//poczatkowe wspolrzedne mrowki
	int x;
	int y;
	char zwrot;
};

#endif