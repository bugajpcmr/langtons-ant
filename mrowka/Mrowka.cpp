#include "Mrowka.h"



Mrowka::Mrowka():x(0),y(0),zwrot('N')
{

}


Mrowka::~Mrowka()
{
}

int Mrowka::GetX()
{
	return x;
}

int Mrowka::GetY()
{
	return y;
}

const char Mrowka::GetZwrot()
{
	return zwrot;
}

void Mrowka::SetX(int x)
{
	this->x = x;
}

void Mrowka::SetY(int y)
{
	this->y = y;
}

void Mrowka::SetZwrot(const char zw)
{
	zwrot = zw;
}
