#include "Engine.h"


Engine::Engine(int i, int j) :Tablica(i, j)
{
	TempTab = new Komorka *[rozmiari + 2];
	for (int i = 0; i < rozmiari + 2; i++)
	{
		TempTab[i] = new Komorka[rozmiarj + 2];
	}
	for (int i = 0; i < rozmiari + 2; i++)
	{
		for (int j = 0; j < rozmiarj + 2; j++)
		{
			TempTab[i][j].SetStan(false);
		}
	}
}


Engine::~Engine()
{
	for (int i = 0; i < rozmiari + 2; i++)
	{
		delete[] TempTab[i];
	}
	delete[] TempTab;
}

void Engine::KopiujTab()
{
	for (int i = 0; i < rozmiari + 2; i++)
	{
		for (int j = 0; j < rozmiarj + 2; j++)
		{
			TempTab[i][j].SetStan(T2D[i][j].GetStan());
		}
	}
}

void Engine::JakiZwrot(bool x)
{
	if (x == 0)
	{
		if (M.GetZwrot() == 'N')
		{
			M.SetZwrot('W');
			M.SetX(M.GetX() - 1);
		}
		else if (M.GetZwrot() == 'E')
		{
			M.SetZwrot('N');
			M.SetY(M.GetY() + 1);
		}
		else if (M.GetZwrot() == 'W')
		{
			M.SetZwrot('S');
			M.SetY(M.GetY() - 1);
		}
		else if (M.GetZwrot() == 'S')
		{
			M.SetZwrot('E');
			M.SetX(M.GetX() + 1);
		}
	}
	else if (x == 1)
	{
		if (M.GetZwrot() == 'N')
		{
			M.SetZwrot('E');
			M.SetX(M.GetX() + 1);
		}
		else if (M.GetZwrot() == 'E')
		{
			M.SetZwrot('S');
			M.SetY(M.GetY() - 1);
		}
		else if (M.GetZwrot() == 'W')
		{
			M.SetZwrot('N');
			M.SetY(M.GetY() + 1);
		}
		else if (M.GetZwrot() == 'S')
		{
			M.SetZwrot('W');
			M.SetX(M.GetX() - 1);
		}
	}
}



void Engine::Analiza()
{
	KopiujTab();
	if (M.GetX() > 0 && M.GetX() < rozmiari && M.GetY() > 0 && M.GetY() < rozmiarj)
	{
		if (TempTab[M.GetX()][M.GetY()].GetStan() == 0)
		{
			T2D[M.GetX()][M.GetY()].SetStan(1);
			JakiZwrot(TempTab[M.GetX()][M.GetY()].GetStan());
		}
		else if (TempTab[M.GetX()][M.GetY()].GetStan() == 1)
		{
			T2D[M.GetX()][M.GetY()].SetStan(0);
			JakiZwrot(TempTab[M.GetX()][M.GetY()].GetStan());
		}
	}
}


