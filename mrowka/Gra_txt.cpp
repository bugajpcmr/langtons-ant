#include "Gra_txt.h"
#include <iostream>
#include <windows.h>			//biblioteka potrzebna do u�ycia funkcji Sleep() aby opoznic wyswietlanie planszy
#include <cstdlib>

Gra_txt::Gra_txt(int i, int j) :Gra(i, j)
{
}


Gra_txt::~Gra_txt()
{
}

void Gra_txt::View()
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	
	system("cls");
	for (int i = 0; i < rozmiari; i++)
	{
		for (int j = 0; j < rozmiarj; j++)
		{
			if (T2D[i][j].GetStan() == 1 && (M.GetX() != i || M.GetY() != j))
			{
				SetConsoleTextAttribute(h, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
				std::cout << "O";
			}
			else if (T2D[i][j].GetStan() == 0 && (M.GetX() != i || M.GetY() != j))
			{
				SetConsoleTextAttribute(h, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
				std::cout << "-";
			}
			else
			{
				SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_INTENSITY);
				std::cout << "M";
			}
		}
		std::cout << std::endl;
	}
	Sleep(500);
}
